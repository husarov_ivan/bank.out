package com.bankout.dataBaseConnection;

import com.bankout.model.Currency;

import java.sql.*;
import java.util.List;

public class DBOperation {
    private static DBOperation instance;

    //  private static List<Connection> connections;
    private DBOperation() {
        Connection connection = null;
        try {
            Class.forName("org.h2.Driver");
            try {
                connection = DriverManager.getConnection("jdbc:h2:mem:BankDB", "", "");
                Statement statement = connection.createStatement();
                String query = "CREATE TABLE CURRENCIES (" +
                        "currency VARCHAR(3) NOT NULL UNIQUE," +
                        "rate DOUBLE NOT NULL);" +
                        "CREATE TABLE ACCOUNT_HOLDERS (" +
                        "holder_id VARCHAR(36) NOT NULL UNIQUE);" +
                        "CREATE TABLE ACCOUNTS (" +
                        "holder_id VARCHAR(36) NOT NULL, " +
                        "currency VARCHAR(3) NOT NULL, " +
                        "id VARCHAR(36) NOT NULL UNIQUE, " +
                        "amount DOUBLE DEFAULT 0, " +
                        "FOREIGN KEY (holder_id) references ACCOUNT_HOLDERS(holder_id)," +
                        "FOREIGN KEY (currency) references CURRENCIES(currency));";
                statement.executeLargeUpdate(query);
                System.out.println("DB created");
                query = "INSERT INTO CURRENCIES VALUES(" +
                        "'UKR',1);" +
                        "INSERT INTO CURRENCIES VALUES(" +
                        "'USD',27.3);" +
                        "INSERT INTO ACCOUNT_HOLDERS VALUES(" +
                        "'12345');" +
                        "INSERT INTO ACCOUNTS VALUES (" +
                        "'12345','USD','98765',276.56)";
                statement.executeLargeUpdate(query);
                System.out.println("Tables created");


            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static DBOperation getInstance() {
        if (instance == null) {
            instance = new DBOperation();
        }
        return instance;
    }

    public static Connection getConnection() throws SQLException {
        return getInstance().getNewConnection();
    }


    private Connection getNewConnection() throws SQLException {
        try {
            Class.forName("org.h2.Driver");
//TODO Connection Pool

            Connection connection = DriverManager.getConnection("jdbc:h2:mem:BankDB", "", "");
            //    connections.add(connection);
            return connection;

        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver exception catch in DB init");
        }
    }
}

