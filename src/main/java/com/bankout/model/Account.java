package com.bankout.model;

import com.bankout.repository.UserRepositoryImpl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Account {
    private String holderID;
    private double amount;
    private final String accountNumber;
    private final Currency accountCurrency;

    public double getAmount() {
        return amount;
    }

    public String getHolderID() {
        return holderID;
    }

    public void changeAmount(double change) {
        this.amount += change;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public Currency getAccountCurrency() {
        return accountCurrency;
    }

    public Account(Currency currency) throws SQLException {
        this.accountCurrency = currency;
        this.amount = 0;
        this.accountNumber = generateUniqAccountNumber();
    }

    public Account(double amount, String accountNumber, Currency accountCurrency, String holder) {
        this.amount = amount;
        this.accountNumber = accountNumber;
        this.accountCurrency = accountCurrency;
        this.holderID = holder;
    }

    private String generateUniqAccountNumber() throws SQLException {
        String accountNumber = new String(UUID.randomUUID().toString());
        if (!UserRepositoryImpl.getInstance().getAccount(accountNumber).isPresent()) {
            return accountNumber;
        } else {
            return generateUniqAccountNumber();
        }
    }

    @Override
    public String toString() {
        return "Account{" +
                "amount=" + amount +
                ", accountNumber='" + accountNumber + '\'' +
                ", accountCurrency=" + accountCurrency +
                '}';
    }
}
