package com.bankout.model;

import com.bankout.BankExceptions.*;
import com.bankout.repository.UserRepositoryImpl;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class User implements AccountHolder {
    private final String userID;
    private Map<Currency, Account> accountsMap;

    @Override
    public String toString() {
        return "User{" +
                "userID='" + userID + '\'' +
                ", accountsMap=" + accountsMap +
                '}';
    }

    public User() {
        this.userID = new String(UUID.randomUUID().toString());
        accountsMap = new HashMap<>();
    }

    public User(String userID, Map<Currency, Account> accountsMap) {
        this.userID = userID;
        this.accountsMap = accountsMap;
    }

    public String addNewAccount(Currency currency) throws AddCurrencyAccountException, SQLException {
        if (accountsMap.containsKey(currency)) {
            throw new AddCurrencyAccountException(currency.toString());
        }
        accountsMap.put(currency, new Account(currency));
        return accountsMap.get(currency).getAccountNumber();
    }

    public void deleteAccount(String accountID) throws NoSuchAccountException {

    }

    public String getUserID() {
        return userID;
    }

    public Map<Currency, Account> getAccountsMap() {
        return accountsMap;
    }

    @Override
    public boolean transaction(String accountNumber, double amount) throws NoSuchAccountException,
            NotEnoughMoneyException, NoAccountForSuchCurrencyException, SQLException, IllegalAmountException, NoSuchUserException {
        if (amount <= 0) {
            throw new IllegalAmountException(amount);
        }
        if (!UserRepositoryImpl.getInstance().getAccount(accountNumber).isPresent()) {
            throw new NoSuchAccountException(accountNumber);
        }
        Account account = UserRepositoryImpl.getInstance().getAccount(accountNumber).get();
        if (!accountsMap.containsKey(account.getAccountCurrency())) {
            throw new NoAccountForSuchCurrencyException(account.getAccountCurrency()
                    .toString());
        }
        Account hereAccount = accountsMap.get(account.getAccountCurrency());
        if (hereAccount.getAmount() < amount) {
            throw new NotEnoughMoneyException(amount - hereAccount.getAmount(), hereAccount.getAccountNumber());
        }
        Optional<User> destinationUser = UserRepositoryImpl.getInstance().getUserByID(account.getHolderID());
        Optional<User> hereUser = UserRepositoryImpl.getInstance().getUserByID(userID);
        if (destinationUser.isPresent() && hereUser.isPresent()) {
            hereAccount.changeAmount(-amount);
            destinationUser.get().getAccountsMap().get(account.getAccountCurrency()).changeAmount(amount);
            UserRepositoryImpl.getInstance().updateUser(hereUser.get());
            UserRepositoryImpl.getInstance().updateUser(destinationUser.get());
            return true;
        } else {
            throw new NoSuchUserException(userID + " " + account.getHolderID());
        }
    }

    @Override
    public boolean convert(Currency from, Currency to, double amount) throws NoAccountForSuchCurrencyException,
            NotEnoughMoneyException, IllegalAmountException {
        if (amount <= 0) {
            throw new IllegalAmountException(amount);
        }
        if (!accountsMap.containsKey(from)) {
            throw new NoAccountForSuchCurrencyException(from.toString());
        }
        if (!accountsMap.containsKey(to)) {
            throw new NoAccountForSuchCurrencyException(to.toString());
        }
        if (accountsMap.get(from).getAmount() < amount) {
            throw new NotEnoughMoneyException(amount - accountsMap.get(from).getAmount(), accountsMap.get(from)
                    .getAccountNumber());
        }
        accountsMap.get(from).changeAmount(-amount);
        accountsMap.get(to).changeAmount(amount * from.getToUAH() / (to.getToUAH()));
        return true;
    }
}
