package com.bankout.model;

import com.bankout.BankExceptions.*;

import java.sql.SQLException;

public interface AccountHolder {
    boolean transaction(String accountNumber, double amount) throws NoSuchAccountException, NotEnoughMoneyException,
            NoAccountForSuchCurrencyException, SQLException, IllegalAmountException, NoSuchUserException;

    boolean convert(Currency from, Currency to, double amount) throws NoSuchAccountException
            , NotEnoughMoneyException, NoAccountForSuchCurrencyException, IllegalAmountException;

}
