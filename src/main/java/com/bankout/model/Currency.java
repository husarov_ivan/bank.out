package com.bankout.model;


import com.bankout.repository.UserRepositoryImpl;

import java.sql.SQLException;

public enum Currency {
    UAH(1), EUR(29.35), USD(26.95), RUR(0.325);
    private double toUAH;

    Currency(double toUAH) {
        this.toUAH = toUAH;
        try {
            UserRepositoryImpl.getInstance().setCurrencyRate(this.toString(), toUAH);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public double getToUAH() {
        return toUAH;
    }

    public void setToUAH(double rate) {
        this.toUAH = rate;
    }


}
