package com.bankout;

import com.bankout.BankExceptions.AddCurrencyAccountException;
import com.bankout.BankExceptions.NoSuchCurrencyException;
import com.bankout.BankExceptions.NoSuchUserException;
import com.bankout.model.Currency;
import com.bankout.model.User;
import com.bankout.repository.UserRepository;
import com.bankout.repository.UserRepositoryImpl;
import com.bankout.service.UserService;
import com.bankout.service.UserServiceImpl;

import java.sql.*;
import java.util.*;

public class Application {

    public static void main(String[] args) throws SQLException, NoSuchCurrencyException, NoSuchUserException, AddCurrencyAccountException {
        Random z = new Random();
        UserServiceImpl service = UserServiceImpl.getInstance();
        User user = service.createUser();
        System.out.println(user);
        service.createNewAccount(user.getUserID(),"USD");
        user = service.getUserByID(user.getUserID());
        System.out.println(user);
//        User americanUser = new User();
//        try {
//            americanUser.addNewAccount(Currency.USD);
//        } catch (AddCurrencyAccountException e) {
//            e.printStackTrace();
//        }
//        UserRepository repository = UserRepositoryImpl.getInstance();
//        //System.out.println(americanUser);
//        repository.createUser(americanUser);
//        List<User> users = repository.getAllUsers().get();
//        System.out.println(users);
//        users.get(0).getAccountsMap().get(Currency.USD).changeAmount(256.7);
//        repository.updateUser(users.get(0));
//        System.out.println(users);
//        repository.deleteUser(users.get(0).getUserID());
//        users = repository.getAllUsers().get();
//        System.out.println(users);


    }
}
