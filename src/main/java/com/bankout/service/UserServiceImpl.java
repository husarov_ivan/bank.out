package com.bankout.service;

import com.bankout.BankExceptions.*;
import com.bankout.model.Account;
import com.bankout.model.Currency;
import com.bankout.model.User;
import com.bankout.repository.AccountRepository;
import com.bankout.repository.UserRepository;
import com.bankout.repository.UserRepositoryImpl;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService, AccountService {
    private static UserServiceImpl instance;
    private UserRepository repository;

    private UserServiceImpl() {
        repository = UserRepositoryImpl.getInstance();
    }

    public static UserServiceImpl getInstance() {
        if (instance == null) {
            instance = new UserServiceImpl();
        }
        return instance;
    }

    @Override
    public boolean transaction(String userID, String destinationAccountNumber, double amount)
            throws NoSuchAccountException, NotEnoughMoneyException, NoAccountForSuchCurrencyException
            , SQLException, NoSuchUserException, IllegalAmountException {
        Optional<User> user = repository.getUserByID(userID);
        if (user.isPresent()) {
            return user.get().transaction(destinationAccountNumber, amount);
        } else {
            throw new NoSuchUserException(userID);
        }
    }

    @Override
    public boolean convert(String userID, String fromCurrency, String toCurrency, double amount)
            throws NotEnoughMoneyException, NoAccountForSuchCurrencyException
            , NoSuchUserException, SQLException, NoSuchCurrencyException, IllegalAmountException {
        try {
            Currency from = Currency.valueOf(fromCurrency);
            Currency to = Currency.valueOf(toCurrency);
            Optional<User> user = repository.getUserByID(userID);
            if (user.isPresent()) {
                user.get().convert(from, to, amount);
                repository.updateUser(user.get());
                return true;
            } else {
                throw new NoSuchUserException(userID);
            }
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new NoSuchCurrencyException(fromCurrency + " " + toCurrency);
        }
    }

    @Override
    public boolean createNewAccount(String userID, String accountCurrency) throws NoSuchCurrencyException
            , NoSuchUserException, SQLException, AddCurrencyAccountException {
        try {
            Currency currency = Currency.valueOf(accountCurrency);
            Optional<User> user = repository.getUserByID(userID);
            if (user.isPresent()) {
                user.get().addNewAccount(currency);
                repository.updateUser(user.get());
                return true;
            } else {
                throw new NoSuchUserException(userID);
            }
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new NoSuchCurrencyException(accountCurrency);
        }
    }

    @Override
    public boolean deleteAccount(String accountID) throws NoSuchAccountException, SQLException, NoSuchUserException {
        Optional<Account> account = ((AccountRepository) repository).getAccount(accountID);
        if (account.isPresent()) {
            Optional<User> user = repository.getUserByID(account.get().getHolderID());
            if (user.isPresent()) {
                user.get().deleteAccount(accountID);
                repository.updateUser(user.get());
                return true;
            } else {
                throw new NoSuchUserException(account.get().getHolderID());
            }
        } else {
            throw new NoSuchAccountException(accountID);
        }
    }

    @Override
    public User getUserByID(String id) throws SQLException, NoSuchUserException {
        Optional<User> user = repository.getUserByID(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new NoSuchUserException(id);
        }
    }

    @Override
    public void deleteUser(String userID) throws SQLException, NoSuchUserException {
        if (repository.getUserByID(userID).isPresent()) {
            repository.deleteUser(userID);
        } else {
            throw new NoSuchUserException(userID);
        }
    }

    @Override
    public User createUser() throws SQLException {
        Optional<User> user = repository.createUser(new User());
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new SQLException("Error occurred while user creation");
        }
    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        Optional<List<User>> users = repository.getAllUsers();
        if (users.isPresent()) {
            return users.get();
        } else {
            throw new SQLException("Error occurred while getting all users");
        }
    }
}
