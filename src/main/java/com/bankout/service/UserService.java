package com.bankout.service;

import com.bankout.BankExceptions.NoAccountForSuchCurrencyException;
import com.bankout.BankExceptions.NoSuchAccountException;
import com.bankout.BankExceptions.NoSuchUserException;
import com.bankout.BankExceptions.NotEnoughMoneyException;
import com.bankout.model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserService {
    User getUserByID(String id) throws SQLException, NoSuchUserException;

    void deleteUser(String userID) throws SQLException, NoSuchUserException;

    User createUser() throws SQLException;

    List<User> getAllUsers() throws SQLException;



    //display all users #
    //get user by id #
    //display all accounts for user #
    //transaction #
    //conversion #
    //create new user #
    //delete existing user #
    //create new currency account for user #
    //delete user account #
}
