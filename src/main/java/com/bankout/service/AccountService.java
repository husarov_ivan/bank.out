package com.bankout.service;

import com.bankout.BankExceptions.*;

import java.sql.SQLException;

public interface AccountService {
    boolean transaction(String userID, String destinationAccountNumber, double amount) throws NoSuchAccountException
            , NotEnoughMoneyException, NoAccountForSuchCurrencyException, SQLException, NoSuchUserException, IllegalAmountException;

    boolean convert(String userID, String fromCurrency, String toCurrency, double amount) throws
            NotEnoughMoneyException, NoAccountForSuchCurrencyException, NoSuchUserException, SQLException, NoSuchCurrencyException, IllegalAmountException;

    boolean createNewAccount(String userID, String accountCurrency) throws
            SQLException, NoSuchUserException, AddCurrencyAccountException, NoSuchCurrencyException;

    boolean deleteAccount(String accountID) throws NoSuchAccountException, SQLException, NoSuchUserException;
}
