package com.bankout.BankExceptions;

public class NoSuchUserException extends OperationException {
    public NoSuchUserException(String id) {
        super("No user expected with id = " + id);
    }
}
