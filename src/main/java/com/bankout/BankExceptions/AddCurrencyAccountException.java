package com.bankout.BankExceptions;

public class AddCurrencyAccountException extends OperationException {
    public AddCurrencyAccountException(String currency) {
        super("User has already had account with " + currency + " currency");
    }
}
