package com.bankout.BankExceptions;

public class IllegalAmountException extends OperationException {
    public <T extends Number>IllegalAmountException(T amount) {
        super("Amount cannot be "+amount.doubleValue()+". It cannot be less than 0!");
    }
}
