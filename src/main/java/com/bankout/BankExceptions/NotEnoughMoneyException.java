package com.bankout.BankExceptions;

public class NotEnoughMoneyException extends OperationException {
    public NotEnoughMoneyException(double change, String account) {
        super("Not enough money on " + account + "for operation. You need at least " + change + " more.");
    }
}
