package com.bankout.BankExceptions;

public class NoAccountForSuchCurrencyException extends OperationException {
    public NoAccountForSuchCurrencyException(String currency) {
        super("No account created for " + currency + " currency.");
    }
}
