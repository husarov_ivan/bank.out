package com.bankout.BankExceptions;

public class NoSuchAccountException extends OperationException {

    public NoSuchAccountException(String account) {
        super("Account " + account + " does not exist.");
    }
}
