package com.bankout.BankExceptions;

public class NoSuchCurrencyException extends OperationException {
    public NoSuchCurrencyException(String currency) {
        super("Cant find " + currency + " currency!");
    }
}
