package com.bankout.repository;

import com.bankout.model.Account;

import java.sql.SQLException;
import java.util.Optional;

public interface AccountRepository {
    Optional<Account> getAccount(String id) throws SQLException;
}
