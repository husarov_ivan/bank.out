package com.bankout.repository;

import com.bankout.model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface UserRepository {
    Optional<User> getUserByID(String id) throws SQLException;
    Optional<User> updateUser(User user) throws SQLException;
    void deleteUser(String userID) throws SQLException;
    Optional<User> createUser(User user) throws SQLException;
    Optional<List<User>> getAllUsers() throws SQLException;
}
