package com.bankout.repository;

import com.bankout.BankExceptions.NoSuchCurrencyException;
import com.bankout.dataBaseConnection.DBOperation;
import com.bankout.model.Account;
import com.bankout.model.Currency;
import com.bankout.model.User;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class UserRepositoryImpl implements UserRepository, AccountRepository, CurrenciesRepository {
    private static UserRepositoryImpl instance;

    private UserRepositoryImpl() {
    }

    public static UserRepositoryImpl getInstance() {
        if (instance == null) {
            instance = new UserRepositoryImpl();
        }
        return instance;
    }

    @Override
    public Optional<User> getUserByID(String id) throws SQLException {
        PreparedStatement statement = null;
        Map<Currency, Account> map = new HashMap<>();
        try (Connection connection = DBOperation.getConnection()) {
            String query = "SELECT * FROM ACCOUNTS WHERE id = ?;";
            statement = connection.prepareStatement(query);
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                map.put(Currency.valueOf(resultSet.getString("currency"))
                        , new Account(resultSet.getDouble("amount")
                                , resultSet.getString("id")
                                , Currency.valueOf(resultSet.getString("currency"))
                                , resultSet.getString("holder_id")));
            }
            List<Currency> currencies = map.values().stream().map(Account::getAccountCurrency).collect(Collectors.toList());
            query = "SELECT * FROM CURRENCIES";
            resultSet = statement.executeQuery(query);
            while (resultSet.next() && currencies.size() != 0) {
                for (Currency currency : currencies) {
                    if (currency.toString().equals(resultSet.getString("currency"))) {
                        currency.setToUAH(resultSet.getDouble("rate"));
                        currencies.remove(currency);
                    }
                }
            }

        } finally {
            if (statement != null && !statement.isClosed()) {
                statement.close();
            }
            return Optional.of(new User(id, map));
        }

    }

    @Override
    public Optional<User> updateUser(User user) throws SQLException {
        try (Connection connection = DBOperation.getConnection(); Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);
            Savepoint save = connection.setSavepoint();
            StringBuilder query = new StringBuilder();
            ResultSet resultSet = statement.executeQuery("SELECT id FROM ACCOUNTS WHERE" +
                    " holder_id='" + user.getUserID() + "';");
            List<String> dbAccs = new ArrayList<>();
            while (resultSet.next()) {
                dbAccs.add(resultSet.getString("id"));
            }
            List<Account> accounts = new ArrayList<>(user.getAccountsMap().values());
            for (Account account : accounts) {
                if (dbAccs.contains(account.getAccountNumber())) {
                    dbAccs.remove(account.getAccountNumber());
                    //update
                    query.append("UPDATE ACCOUNTS SET amount=").append(account.getAmount()).append(" WHERE id='").append(account.getAccountNumber()).append("';");
                } else {
                    //create
                    query.append("INSERT INTO ACCOUNTS VALUES ('").append(user.getUserID()).append("','")
                            .append(account.getAccountCurrency().toString())
                            .append("','").append(account.getAccountNumber())
                            .append("',").append(account.getAmount()).append("); ");
                }

            }
            //tails removing
            for (String dbAcc : dbAccs) {
                query.append("DELETE FROM ACCOUNTS WHERE id='").append(dbAcc).append("'; ");
            }
            try {
           //     System.out.println(query.toString());
                statement.executeLargeUpdate(query.toString());
                connection.commit();
                connection.setAutoCommit(true);
                return getUserByID(user.getUserID());
            } catch (SQLException ex) {
                connection.rollback(save);
                throw new SQLException("User cannot be updated!");
            }
        }
    }

    @Override
    public void deleteUser(String userID) throws SQLException {
        try (Connection connection = DBOperation.getConnection(); Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);
            Savepoint save = connection.setSavepoint();
            try {
                String query = "DELETE FROM ACCOUNTS WHERE holder_id='" + userID + "'; " +
                        "DELETE FROM ACCOUNT_HOLDERS WHERE holder_id='" + userID + "';";
                statement.executeUpdate(query);
                connection.commit();
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                connection.rollback(save);
                throw new SQLException("User haven`t been deleted!");
            }
        }
    }

    @Override
    public Optional<User> createUser(User user) throws SQLException {
        try (Connection connection = DBOperation.getConnection(); Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);
            Savepoint save = connection.setSavepoint();
            Map<String, Map<Currency, Account>> accountHolders = new HashMap<>();
            StringBuilder query = new StringBuilder("INSERT INTO ACCOUNT_HOLDERS VALUES('")
                    .append(user.getUserID()).append("'); ");
            List<Account> accounts = new ArrayList<>(user.getAccountsMap().values());
            for (Account account : accounts) {
                query.append("INSERT INTO ACCOUNTS VALUES ('").append(user.getUserID()).append("','")
                        .append(account.getAccountCurrency().toString())
                        .append("','").append(account.getAccountNumber())
                        .append("',").append(account.getAmount()).append("); ");
            }
            try {
                statement.executeLargeUpdate(query.toString());
                connection.commit();
                connection.setAutoCommit(true);
                return getUserByID(user.getUserID());
            } catch (SQLException ex) {
                connection.rollback(save);
                throw new SQLException("User cannot be created!");
            }
        }
    }

    @Override
    public Optional<List<User>> getAllUsers() throws SQLException {
        List<User> users = null;
        try (Connection connection = DBOperation.getConnection(); Statement statement = connection.createStatement()) {
            Map<String, Map<Currency, Account>> accountHolders = new HashMap<>();
            users = new ArrayList<>();
            String query = "SELECT * FROM ACCOUNTS;";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if (!accountHolders.containsKey(resultSet.getString("holder_id"))) {
                    accountHolders.put(resultSet.getString("holder_id"), new HashMap<>());
                }
                accountHolders.get(resultSet.getString("holder_id"))
                        .put(Currency.valueOf(resultSet.getString("currency"))
                                , new Account(resultSet.getDouble("amount")
                                        , resultSet.getString("id")
                                        , Currency.valueOf(resultSet.getString("currency"))
                                        , resultSet.getString("holder_id")));
            }
            query = "SELECT * FROM CURRENCIES";
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                for (Currency currency : Currency.values()) {
                    if (currency.toString().equals(resultSet.getString("currency"))) {
                        currency.setToUAH(resultSet.getDouble("rate"));
                    }
                }
            }
            List<User> finalUsers = users;
            accountHolders.forEach((K, V) -> finalUsers.add(new User(K, V)));
            return Optional.ofNullable(users);
        }
    }//get account by number and check if such account exists

    @Override
    public Optional<Account> getAccount(String id) throws SQLException {
        Account account = null;
        PreparedStatement statement = null;
        try (Connection connection = DBOperation.getConnection()) {
            String query = "SELECT * FROM ACCOUNTS WHERE id = ?;";
            statement = connection.prepareStatement(query);
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                account = new Account(resultSet.getDouble("amount")
                        , resultSet.getString("id")
                        , Currency.valueOf(resultSet.getString("currency"))
                        , resultSet.getString("holder_id"));
            }
        } finally {
            if (statement != null && !statement.isClosed()) {
                statement.close();
            }
            return Optional.ofNullable(account);
        }
    }


    @Override
    public void setCurrencyRate(String currency, double rate) throws SQLException {
        try (Connection connection = DBOperation.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT rate FROM CURRENCIES WHERE" +
                    " currency='" + currency + "';");
            List<String> db = new ArrayList<>();
            Double rateDB = null;
            while (resultSet.next()) {
                rateDB = resultSet.getDouble("rate");
            }
            if (rateDB == null) {
                statement.executeUpdate("INSERT INTO CURRENCIES VALUES('" + currency + "'," + rate + ");");
            } else {
                statement.executeUpdate("UPDATE CURRENCIES SET rate=" + rate + " WHERE currency='" + currency + "';");
            }

        }
    }

    @Override
    public double getCurrencyRate(String currency) throws SQLException, NoSuchCurrencyException {
        Double rate = null;
        PreparedStatement statement = null;
        try (Connection connection = DBOperation.getConnection()) {
            String query = "SELECT rate FROM CURRENCIES WHERE currency = ?;";
            statement = connection.prepareStatement(query);
            statement.setString(1, currency);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                rate = resultSet.getDouble("currency");
            }
        } finally {
            if (statement != null && !statement.isClosed()) {
                statement.close();
            }
            if (rate == null) {
                throw new NoSuchCurrencyException(currency);
            }
            return rate;
        }

    }
}
