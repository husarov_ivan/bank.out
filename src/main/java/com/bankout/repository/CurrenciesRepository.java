package com.bankout.repository;

import com.bankout.BankExceptions.NoSuchCurrencyException;

import java.sql.SQLException;

public interface CurrenciesRepository {
    void setCurrencyRate(String currency, double rate) throws SQLException;

    double getCurrencyRate(String currency) throws SQLException, NoSuchCurrencyException;

}
